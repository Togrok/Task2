package employees;

import java.sql.*;
import java.util.Scanner;

public class EmployeesDB {
    private final String jdbcDriver = "com.mysql.jdbc.Driver";
    private final String dbUrl = "jdbc:mysql://localhost:3306/?useSSL=true";
    private final String user = "root";

    private final String employeeFieldName = "name";
    private final String employeeFieldSalary = "salary";
    private final String employeeFieldId = "id";

    private Connection dbConnection;

    public EmployeesDB() {
        setConnection();
        createEmployeesDB();
    }

    private void checkDriver() {
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Driver was not found");
        }
    }

    private void setConnection() {
        checkDriver();
        System.out.println("Connecting to mySQL root user, please enter password");
        Scanner scanPassword = new Scanner(System.in);
        final String rootPassword = scanPassword.next();
        Connection newConnection = null;
        try {
            newConnection = DriverManager.getConnection(dbUrl,user,rootPassword);
        } catch (SQLException e) {
            throw new IllegalArgumentException("Database access error or password is wrong");
        }
        dbConnection = newConnection;
    }

    private Statement getStatement() {
        try {
            return dbConnection.createStatement();
        } catch (SQLException e) {
            throw  new IllegalStateException("Database access error or connection is closed");
        }
    }

    private void createEmployeeTable() throws SQLException{
        Statement runStmt = dbConnection.createStatement();
        runStmt.executeQuery("SET SQL_MODE = ''");
        runStmt.executeUpdate("CREATE TABLE IF NOT EXISTS EMPLOYEES.EMPLOYEE"
                + "(" + employeeFieldId + " INTEGER not NULL, " +
                employeeFieldName + " VARCHAR(255) not NULL, " +
                employeeFieldSalary +" INTEGER not NULL, " +
                " PRIMARY KEY ( id ))");
    }

    private void createEmployeesDB() {
        try {
            Statement runStmt = dbConnection.createStatement();
            runStmt.executeUpdate("CREATE DATABASE IF NOT EXISTS EMPLOYEES");
            createEmployeeTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void printEmployee() {
        Statement readStmt = getStatement();
        String selectAllQuery = "SELECT * FROM EMPLOYEES.EMPLOYEE";
        try {
            ResultSet queryRes = readStmt.executeQuery(selectAllQuery);
            while(queryRes.next()) {
                System.out.println(queryRes.getString(employeeFieldId) + " "
                        + queryRes.getString(employeeFieldName) + " "
                        + queryRes.getString(employeeFieldSalary));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet readFromEmployee(String sqlQuery) {
        Statement readStmt = getStatement();
        ResultSet queryResultSet = null;
        try {
            queryResultSet = readStmt.executeQuery(sqlQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return queryResultSet;
    }

    public void updateEmployeeTable(int id, String name, double salary) {
        Statement updateTableStmt = getStatement();
        String updateQuery = "INSERT INTO EMPLOYEES.EMPLOYEE (id, name, salary) " +
                "VALUES ('" + id + "', '" + name + "', '" + salary + "')";
        try {
            updateTableStmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteFromEmployee(String deleteCondition){
        Statement deleteStmt = getStatement();
        String deleteQuery = "DELETE FROM EMPLOYEES.EMPLOYEE WHERE " + deleteCondition;
        try {
            deleteStmt.executeUpdate(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
